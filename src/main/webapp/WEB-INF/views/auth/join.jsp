<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- 검증 cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!-- 사용자 지정 검증 폼 넣기 -->
<script src="../../resources/js/joinForm.js"></script>
    <title>가입</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="../../resources/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.css">
    
    <link rel="stylesheet" href="../../resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../resources/css/magnific-popup.css">

    <link rel="stylesheet" href="../../resources/css/aos.css">

    <link rel="stylesheet" href="../../resources/css/ionicons.min.css">

    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../resources/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="../../resources/css/flaticon.css">
    <link rel="stylesheet" href="../../resources/css/icomoon.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/">프로젝트</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	       
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item bread-item" style="background-image: url(../../resources/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center justify-content-center" data-scrollax-parent="true">

            <div class="col-md-8 mt-5 text-center col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">처음으로</a></span></p>
	            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">가입을 시작합니다.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">가입하기</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-3">
           
          </div>
          <div class="col-md-3">
            
          </div>
          <div class="col-md-3">
           
          </div>
          
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
            <form id = "joinForm" action = "/auth/signUp" method="post">
              <div class="form-group">
              	<c:choose>
				<c:when test = "${empty alert}"> <!-- checkId값 -->
				<label>아이디: </label>
                <input type="text" class="form-control"id="uid" name="uid">
                <small id="uid" class="text-info"></small>
                </c:when>
				<c:otherwise>
				<label>아이디: </label>
				<input type="text" class="form-control" id="uid" name="uid" placeholder="이미 존재하는 아이디">
				<small id="uid" class="text-info"></small>
				</c:otherwise>
				</c:choose>
              </div>
             	<div class="form-pw">
				<label>비밀번호: </label><input type="password" id="password" name="password" class="form-control">
				<small id="password" class="text-info"></small>
				</div>
				<div class="form-name">
				<label>이름:</label><input type="text" id="name" name="name" class="form-control">
				<small id="name" class="text-info"></small>
				</div>
				<div class="form-email">
				<label>이메일:</label><input type="email" id="email" name="email" class="form-control">
				<small id="email" class="text-info"></small>
				</div>
				<div class="form-position">
				<label>직책: </label><input type="text" id="position" name="position" class="form-control">
				<small id="position" class="text-info"></small>
				</div>
				<div class="form-dept">
				<label>부서: </label><input type="text" id="dept" name="dept" class="form-control">
				<small id="dept" class="text-info"></small>
				</div>

				<button type="submit" class="btn btn-primary">가입</button>
            </form>
          
          </div>
<!-- 
          <div class="col-md-6" id="map"></div> 
 -->
        </div>
      </div>
    </section>

 
   <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 프로젝트 작성자 <i class="icon-heart" aria-hidden="true"></i> <a href="https://colorlib.com" target="_blank">주명훈</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


 
  <script src="../../resources/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../../resources/js/popper.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/jquery.easing.1.3.js"></script>
  <script src="../../resources/js/jquery.waypoints.min.js"></script>
  <script src="../../resources/js/jquery.stellar.min.js"></script>
  <script src="../../resources/js/owl.carousel.min.js"></script>
  <script src="../../resources/js/jquery.magnific-popup.min.js"></script>
  <script src="../../resources/js/aos.js"></script>
  <script src="../../resources/js/jquery.animateNumber.min.js"></script>
  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/jquery.timepicker.min.js"></script>
  <script src="../../resources/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="../../resources/js/google-map.js"></script>
  <script src="../../resources/js/main.js"></script>
    
  </body>
</html>