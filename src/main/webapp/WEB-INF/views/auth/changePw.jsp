<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
	<script src="../../resources/js/changePw.js"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="../../resources/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.css">
    
    <link rel="stylesheet" href="../../resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../resources/css/magnific-popup.css">

    <link rel="stylesheet" href="../../resources/css/aos.css">

    <link rel="stylesheet" href="../../resources/css/ionicons.min.css">

    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../resources/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="../../resources/css/flaticon.css">
    <link rel="stylesheet" href="../../resources/css/icomoon.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/">프로젝트</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	       
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item bread-item" style="background-image: url(resources/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center justify-content-center" data-scrollax-parent="true">

            <div class="col-md-8 mt-5 text-center col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">처음으로</a></span></p>
	            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">비밀번호를 변경합니다.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">변경하기</h2>
          </div>
          <div class="w-100"></div>
          <div class="col-md-3">
           
          </div>
          <div class="col-md-3">
            
          </div>
          <div class="col-md-3">
           
          </div>
          
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
          <form id = "changePw" action = "/auth/changePw" method="post">
              <div class="form-group">
              	<label>기존 비밀번호: </label><input type="password" id="password_now" name="password_now">
              	 <small id="password_now" class="text-info"></small>
              </div>
              <div class="form-pw">
				<label>변경할 비밀번호:</label><input type="password" id="password_after" name="password_after">
				 <small id="password_after" class="text-info"></small>
			  </div>
			  <div class="form-name">
				<label>비밀번호 확인:</label><input type="password" id="retype_password_after" name="retype_password_after">
				 <small id="retype_password_after" class="text-info"></small>
		      </div>
				<button type="submit" class="btn btn-primary">변경하기</button>
            </form>
          	
          </div>
<!-- 
          <div class="col-md-6" id="map"></div> 
 -->
        </div>
      </div>
    </section>

 
   <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 프로젝트 작성자 <i class="icon-heart" aria-hidden="true"></i> <a href="https://colorlib.com" target="_blank">주명훈</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  
  <script src="../../resources/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../../resources/js/popper.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/jquery.easing.1.3.js"></script>
  <script src="../../resources/js/jquery.waypoints.min.js"></script>
  <script src="../../resources/js/jquery.stellar.min.js"></script>
  <script src="../../resources/js/owl.carousel.min.js"></script>
  <script src="../../resources/js/jquery.magnific-popup.min.js"></script>
  <script src="../../resources/js/aos.js"></script>
  <script src="../../resources/js/jquery.animateNumber.min.js"></script>
  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/jquery.timepicker.min.js"></script>
  <script src="../../resources/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="../../resources/js/google-map.js"></script>
  <script src="../../resources/js/main.js"></script>
    
  </body>
</html>