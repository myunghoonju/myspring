<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>WebHost - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="../../resources/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.css">
    
    <link rel="stylesheet" href="../../resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../resources/css/magnific-popup.css">

    <link rel="stylesheet" href="../../resources/css/aos.css">

    <link rel="stylesheet" href="../../resources/css/ionicons.min.css">

    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../resources/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="../../resources/css/flaticon.css">
    <link rel="stylesheet" href="../../resources/css/icomoon.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/"> 프로젝트</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	    
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item bread-item" style="background-image: url(../../resources/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center justify-content-center" data-scrollax-parent="true">

            <div class="col-md-8 mt-5 text-center col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">처음으로</a></span> </p>
	            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">보안서약서</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="ftco-section contact-section ftco-degree-bg">
      <div class="container">
        <div class="row d-flex mb-5 contact-info">
          <div class="col-md-12 mb-4">
            <h2 class="h4">서약하세요</h2>
          </div>    
        </div>
        <div class="row block-9">
          <div class="col-md-6 pr-md-5">
    <div>
	<textarea rows="25" cols="120">
더블다운인터액티브(이하 '회사')의 업무를 수행하는 협력회사, 상주파견 업체, 기타 외부업체 직원들은 본 서약서가 근무기간뿐 아니라 파견해제 후에도 일정기간 적용될 수 있음을 인식하고 숙독하신 후 서명하기 바랍니다.

1. 나는 회사로부터 취득한 모든 정보를 회사 관련 업무에 한해 이용할 것이며, 타기업의 보호대상 정보를 회사 내 보관치 않겠다.
2. 나는 회사로부터 제공받은 정보자산(서류, 사진, 전자파일, 저장매체, 전산장비 등)을 무단변조, 복사, 훼손, 분실 등으로부터 안전하게 관리하겠다.
3. 나는 상대가 누구이건 간에(회사직원, 고객 혹은 계약직 사원 등) 알 필요가 없는 자에게 회사 혹은 제3자의 소유정보를 누설하지 않겠다.
4. 나는 명백히 허가 받지 않은 정보나 시설에 접근하지 않으며, 회사관련 업무를 수행할 때만 사내 데이터 처리시설을 이용하고, 이 시설 내에 사적 정보를 보관치 않겠다.
5. 나는 회사에서 승인 받지 않은 프로그램, 정보저장 매체(USB, Zip Drive, CD-ROM, 외장 HDD 등)을 회사 내에서 사용치 않겠다.
6. 나는 회사소유 정보자산을 외부로 발신 시 회사의 통제절차를 준수할 것이며, 회사가 정보 자산을 보호하기 위해 회사통신망을 통해 수 발신되는 전자문서를 점검할 수 있음을 알고 있다.
7. 나는 업무와 관련한 문서의 생성, 사용, 폐기시 문서권한 관리 규칙에 의거 규정을 준수하겠다
8. 나는 나에게 할당된 사용자 ID, 패스워드, 출입증을 타인과 공동사용 또는 누설치 않겠다.
9. 나는 회사의 정보보호 정책 및 지침, 절차를 준수하겠다.
10. 나는 회사에서 나의 수행 업무와 관련되어 감사를 시행하는 경우 이에 적극 협조하겠다.
11. 나는 퇴직(프로젝트 종료, 파견 해제) 시 회사에서 제공받은 회사소유 모든 정보자산을 반드시 반납할 것이며, 이후에도 회사의 모든 영업비밀은 물론이고 기타 누설됨으로 인하여 회사에 손해가 될 수 있는 각종 정보에 대하여는 이를 일절 누설치 않겠다.

 상기사항을 숙지하고 이를 성실히 준수할 것을 동의하며 서약서의 보안사항을 위반하였을 경우에는 “부정경쟁방지 및 영업비밀보호에 관한 법률” “정보통신망이용촉진 및 정보보호 등에 관한 법률” 등 관련법령에 의한 민/형사상의 책임이외에도,
 회사의 사규나 관련 규정에 따른 징계조치 등 어떠한 불이익도 감수할 것이며 회사에 끼친 손해에 대해 지체 없이 변상/복구할 것을 서약합니다.
	</textarea>
	</div>
<form action="/auth/join" id=actionForm method="post">
<select id="agree" name = "agree" class="form-control">
	<option value="true">동의 및 서약</option>
	<option value="false">서약거부</option>
</select>
<button class="btn btn-default">접속</button>
<button type = "button" name = "back" id = "back"><a href = "/">이전</button>
</form>
          
          </div>
<!-- 
          <div class="col-md-6" id="map"></div> 
 -->
        </div>
      </div>
    </section>

   <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 프로젝트 작성자 <i class="icon-heart" aria-hidden="true"></i> <a href="https://colorlib.com" target="_blank">주명훈</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="../../resources/js/jquery.min.js"></script>
  <script src="../../resources/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../../resources/js/popper.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/jquery.easing.1.3.js"></script>
  <script src="../../resources/js/jquery.waypoints.min.js"></script>
  <script src="../../resources/js/jquery.stellar.min.js"></script>
  <script src="../../resources/js/owl.carousel.min.js"></script>
  <script src="../../resources/js/jquery.magnific-popup.min.js"></script>
  <script src="../../resources/js/aos.js"></script>
  <script src="../../resources/js/jquery.animateNumber.min.js"></script>
  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/jquery.timepicker.min.js"></script>
  <script src="../../resources/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="../../resources/js/google-map.js"></script>
  <script src="../../resources/js/main.js"></script>
    
  </body>
</html>