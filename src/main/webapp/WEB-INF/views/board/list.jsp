<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix = "c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>게시글 목록 페이지</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="../../resources/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.css">
    
    <link rel="stylesheet" href="../../resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../resources/css/magnific-popup.css">

    <link rel="stylesheet" href="../../resources/css/aos.css">

    <link rel="stylesheet" href="../../resources/css/ionicons.min.css">

    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../resources/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="../../resources/css/flaticon.css">
    <link rel="stylesheet" href="../../resources/css/icomoon.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
    
<script>
	$(function(){
		var searchForm = $("#searchForm");
		$('#searchButton').click(function(e){
			searchForm.find("input[name='words']").val();
			searchForm.find("input[name='page']").val();
			searchForm.submit();
		})
		$('#back').click(function(){
			location.href = '/';
		})
	})
</script>    
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/">프로젝트</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">

	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item bread-item" style="background-image: url(resources/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center justify-content-center" data-scrollax-parent="true">

            <div class="col-md-8 mt-5 text-center col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/auth/signUp">파일목록</a></span></p>
	            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">게시판 목록입니다.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>


    <section class="ftco-section">
    	<div class="container">
    	 <div class="row d-flex mb-5 contact-info">
          <div class="col-md-3">
            
          </div>
          <div class="col-md-3">
           
          </div>
          
        </div>
    		<div class="row">
    			<div class="col-md-12 ftco-animate">
    				<div class="table-responsive">
	    				<table class="table">
						    <thead class="thead-primary">
						      <tr>
						        <th>글번호</th>
						        <th>글의제목</th>
						        <th>작성자</th>
						        <th>소속부서</th>
						        <th>이메일</th>
						        <th>조회수</th>
						        <th>등록일</th>
						        <th>수정일</th>
						      </tr>
						    </thead>
						    <tbody>
					<c:forEach var = "list" items = "${list}">
						<tr>
						<td>${list.bno}</td>
						<td><a href = "/board/read?bno=${list.bno}">${list.title}</a>
						<c:if test = "${list.reply_count != 0}">[${list.reply_count}]</c:if>
						</td>
						<td>${list.uid}</td>
						<td>${list.dept}</td>
						<td>${list.email}</td>
						<td>${list.board_hit}</td>
						<td><fmt:formatDate value="${list.register_date}" type="date"  /></td> <!-- type="date","time","both" 또는 지정가능 -->
						<td><fmt:formatDate value="${list.update_date}" type="date" /></td>
						</tr>
					</c:forEach>
						    </tbody>
						  </table>
		<!-- search! -->
		<div class = "search">
		<form action = "" id = "searchForm" method = "get">
		<select name = "searchType">
			<option value = "" >선택하세요</option>
			<option value = "T" <c:out value = "${pagevo.cri.searchType eq 'T'?'selected':''}"/>>제목</option>
			<option value = "W" <c:out value = "${pagevo.cri.searchType eq 'W'?'selected':''}"/>>작성자</option>
			<option value = "D" <c:out value = "${pagevo.cri.searchType eq 'D'?'selected':''}"/>>소속부서</option>
			<option value = "T W" <c:out value = "${pagevo.cri.searchType eq 'T W'?'selected':''}"/>>제목/작성자</option>
			<option value = "T D" <c:out value = "${pagevo.cri.searchType eq 'T D'?'selected':''}"/>>제목/소속부서</option>
			<option value = "W D" <c:out value = "${pagevo.cri.searchType eq 'W D'?'selected':''}"/>>작성자/소속부서</option>
			<option value = "T W D" <c:out value = "${pagevo.cri.searchType eq 'T W D'?'selected':''}"/>>모두검색</option>
		</select>
			<input type = "text" name= "words" value  = "${pagevo.cri.words}">
			<input type = "hidden" name= "page" value  = 1>
			<button type = "button" id = "searchButton">검색하기</button>
		</form>						  
						  
						  
						  
					  </div>
    			</div>
    		</div>
    	</div>
    </section>

    <section class="ftco-section bg-light">
		<div class="row mt-5">
          <div class="col text-center">
            <div class="block-27">
              <ul>
 			<c:choose>
 				<c:when test = "${pagevo.prev eq false}">
 				<li class = "page_button_prev">이전</li>
 				</c:when>
 				<c:otherwise>
				<li class = "page_button_prev">
				<a href = "/board/list${pagevo.make_query(pagevo.start_page -1)}">이전</a>
				</li>
				</c:otherwise>
			</c:choose>
			<c:forEach begin = "${pagevo.start_page}" end = "${pagevo.end_page}" var ="index">
				<li class = "page_button">
				<a href = "/board/list${pagevo.make_query(index)}">${index}</a>
				</li>
			</c:forEach>
			<c:if test = "${pagevo.next && pagevo.end_page >0}">
				<li class ="page_button_next">
				<a href = "/board/list${pagevo.make_query(pagevo.end_page + 1)}">다음</a>
				</li>
			</c:if>
		 
			</ul>
            </div>
          </div>
        </div>
        
        <p class = "board_form_sec" align="center">
		<button type="submit" class="btn btn-primary">
			<a href = "/board/write">새로운 글 작성하기</a>
		</button>
		<button type = "button" id = "back">처음으로</button>
		</p>
        
        
        
    </section>

    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 프로젝트 작성자 <i class="icon-heart" aria-hidden="true"></i> <a href="https://colorlib.com" target="_blank">주명훈</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="../../resources/js/jquery.min.js"></script>
  <script src="../../resources/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../../resources/js/popper.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/jquery.easing.1.3.js"></script>
  <script src="../../resources/js/jquery.waypoints.min.js"></script>
  <script src="../../resources/js/jquery.stellar.min.js"></script>
  <script src="../../resources/js/owl.carousel.min.js"></script>
  <script src="../../resources/js/jquery.magnific-popup.min.js"></script>
  <script src="../../resources/js/aos.js"></script>
  <script src="../../resources/js/jquery.animateNumber.min.js"></script>
  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/jquery.timepicker.min.js"></script>
  <script src="../../resources/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="../../resources/js/google-map.js"></script>
  <script src="../../resources/js/main.js"></script>
    
  </body>
</html>