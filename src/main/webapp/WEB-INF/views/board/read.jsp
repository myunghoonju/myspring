<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>WebHost - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,,500,600,700" rel="stylesheet">

    <link rel="stylesheet" href="../../resources/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="../../resources/css/animate.css">
    
    <link rel="stylesheet" href="../../resources/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../resources/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../resources/css/magnific-popup.css">

    <link rel="stylesheet" href="../../resources/css/aos.css">

    <link rel="stylesheet" href="../../resources/css/ionicons.min.css">

    <link rel="stylesheet" href="../../resources/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../resources/css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="../../resources/css/flaticon.css">
    <link rel="stylesheet" href="../../resources/css/icomoon.css">
    <link rel="stylesheet" href="../../resources/css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src = "/resources/js/reply.js"></script>    
<script>
	//답글등록
$(function() {

	$('#insert_reply').click(function(){
		if($('#reply_content').val().trim() == ""){
		alert("내용을 입력하세요.");
		$('#reply_content').focus();
		return false;
	}
	
	var bno = $('#bno').val();
	var uid = $('#uid').val();
	var reply_content = $('#reply_content').val();
	
	var param = {
			bno : bno,
			uid : uid,
			bnogroup : 0,
			sort : 0,
			depth : 0,
			content : reply_content		
	};
	replyService.add(param,function(result){
		if(result.valueOf() == 'success'){
			alert("게시물에 답글을 입력하였습니다.");
		}
	})
	
	window.location.href = "/board/list"
	
	})
	
});
</script>
<script>
/*답변의 답변 입력*/

function replyModal(value){
	var modalA = $('#myModal');
	
	modalA.show();
	
	
	$('#modal_close').click(function(){
		var bno = $('#bno').val();
		var uid = $('#uid').val();
		var bnogroup = value;
		var re_reply_content= $('#re_reply_content').val();
	
		var param = {
				bno : bno,
				uid : uid,
				bnogroup : bnogroup,
				sort : 1,
				depth : 1,
				content : re_reply_content		
		};
	
		replyService.add(param,function(result){
			if(result.valueOf() == 'success')
				alert("해당 답글에 대한 글을 입력하였습니다.");
		})
	
		window.location.href = "/board/list"
	})

}
	
</script>
<script>
//답변수정
function editModal(value){
	$('#editModal').show();
	$('#editmodal_close').click(function(){
		var bno = $('#bno').val();
		var bnogroup = value;
		var reply_content_edit = $('#reply_content_edit').val();
	
		var param = {
			bno : bno,
			bnogroup : value,
			content : reply_content_edit	
			};
		replyService.update(param,function(result){
			if(result.valueOf() == 'success'){
				alert("해당 글을 수정하였습니다.");
			}
		})
		
		window.location.href = "/board/read?bno=" + bno
	})

}
</script>
<script>
//답변삭제
function confirmDelete(value){
	alert("해당 글을 삭제합니다.");
	var bno = $('#bno').val();
	var bnogroup = value;
	
	var param = {
			bno : bno,
			bnogroup : bnogroup
		}
	replyService.remove(param,function(result){
		if(result.valueOf() == 'success')
			alert("해당 글을 삭제하였습니다.");
	})
	
	window.location.href = "/board/list"
	
}

</script>

<script>
	$(function(){
		var changeForm = $("#changeForm");
		
		
		$("#edit").click(function(){
			changeForm.attr("action","/board/edit");
			changeForm.submit();
		})
		$("#delete").on("click",function(){
			changeForm.attr("method","POST");
			changeForm.attr("action","/board/delete");
			changeForm.submit();			
		})
		$("#list").on("click",function(){
			changeForm.attr("method","get");
			changeForm.attr("action","/board/list");
			changeForm.submit();			
		})
});
</script>
    
    
    
  </head>
  <body>
    
	  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="/">프로젝트</a>
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	      
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->

    <section class="home-slider owl-carousel">
      <div class="slider-item bread-item" style="background-image: url(../../resources/images/bg_1.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container-fluid">
          <div class="row slider-text align-items-center justify-content-center" data-scrollax-parent="true">

            <div class="col-md-8 mt-5 text-center col-sm-12 ftco-animate" data-scrollax=" properties: { translateY: '70%' }">
              <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="/">처음으로</a></span> <span class="mr-2"><a href="/board/list">게시판</a></span></p>
	            <h1 class="mb-3 bread" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">선택하신 글의 내용입니다.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>
  
    <section class="ftco-section">
		    <div class="about-author d-flex p-5 bg-light">
		    <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">상세 게시글</h3>
                <form id = "viewFrom" action="" method ="post" class="p-5 bg-light">
                  <div class="form-group">
                    <label for="name">작성자:</label>
                    		${detail.uid}
                  </div>
                  <div class="form-group">
                    <label for="email">글분류: </label>
                   	 ${detail.category}
                  </div>
                  <div class="form-group">
                    <label for="website">제목:</label>
                   <input name = "title" placeholder="${detail.title}" readonly="readonly">
                  </div>

                  <div class="form-group">
                    <label for="message">내용:</label>
                    <textarea name="content"  cols="100" rows="1" class="form-control" placeholder="${detail.content}" readonly="readonly"></textarea>
                  </div>
                  <div class="form-group">
                   <button type="button" data-move="edit" id = "edit">수정</button>
				   <button type="button" data-move="delete" id = "delete">삭제</button>
				   <button type="button" data-move="list" id = "list">목록</button>
                  </div>

                </form>
                
<form id = "changeForm" >
	<input type = "hidden" id = "bno" name = "bno" value = "${detail.bno}">
	<input type = "hidden" id = "uid" name = "uid" value = "${detail.uid}"> 
	<input type = "hidden" id = "title" name = "title" value = "${detail.title}">
	<input type = "hidden" id = "content" name = "content" value = "${detail.content}">
</form>   

<!-- 모달창 -->

<div id="myModal" class="modal" align="center">
	<div>
  		<p>답변에 대한 답글을 입력하세요</p>
  		<textarea id="re_reply_content" name ="re_reply_content"></textarea>
  		<button type="button"  id = "modal_close">입력</button>
  	</div>
</div>

<!-- 수정 모달창 -->

<div id="editModal" class="modal" align="center">
	<div>
		<div>
  			<p>수정내용을 입력하세요</p>
  			<textarea id="reply_content_edit" name ="reply_content_edit"></textarea>
  			<button type="button"  id = "editmodal_close">수정</button>
  		</div>
  	</div>
</div>

              </div>
            
            </div>
			 <div class="comment-form-wrap pt-5">
                <h3 class="mb-5">답변달기</h3>
               	<textarea id="reply_content" name = "reply_content"></textarea>
				<button type= "button" id = "insert_reply">답변등록</button>
              </div>
            </div>

          </div> <!-- .col-md-8 -->




            <div class="pt-5 mt-5">
              <h3 class="mb-5">답변목록</h3>
              <ul class="comment-list">
              <c:forEach  items = "${replyList}" var = "replies" varStatus="status">
				<c:if test = "${replies.depth != 1}">
                <li class="comment">
                  <div class="vcard bio">
                    <img src="../../resources/images/ryan.jpg" alt="Image placeholder">
                  </div>
                  <div class="comment-body">
                 	작성자: ${replies.uid}<br>
					등록일: <fmt:formatDate value="${replies.register_date}" type="date"  /><br>
					<c:choose>
						<c:when test="${replies.update_date eq null}">
							수정일: 최초등록<br>
						</c:when>
					<c:otherwise>
						수정일: <fmt:formatDate value="${replies.update_date}" type="date"/><br>
					</c:otherwise>
					</c:choose>
						${replies.content}<br>
						<button type="button" onclick="replyModal('${replies.bnogroup}');">답글</button>
						<button type = "button" onclick = "editModal('${replies.bnogroup}');">수정하기</button>
						<button type = "button" onclick = "confirmDelete('${replies.bnogroup}');">삭제하기</button>

                  </div>
                </li>
				</c:if>
				<c:if test = "${replies.depth >= 1}">
                  <ul class="children">
                    <li class="comment">
                    <div class="vcard bio">
                    <img src="../../resources/images/apeach.jpg" alt="Image placeholder">
                  </div>
                      <div class="comment-body">
                      	작성자: ${replies.uid} &nbsp;등록일: <fmt:formatDate value="${replies.register_date}" type="date"  />
						<c:choose>
							<c:when test="${replies.update_date eq null}">
								수정일: 최초등록 <br>
							</c:when>
							<c:otherwise>
							수정일: <fmt:formatDate value="${replies.update_date}" type="date"/><br>
							</c:otherwise>
						</c:choose>
							&nbsp;&nbsp;&nbsp;${replies.content}
                      </div>
                	</li>
                </ul>
				</c:if>
				</c:forEach>
              </ul>
            </div>
              <!-- END comment-list -->
              
              <div class="comment-form-wrap pt-5">
              </div>


           <!-- .col-md-8 -->
          <div class="col-md-4 sidebar ftco-animate">
            <div class="sidebar-box">
                          </div>
            <div class="sidebar-box ftco-animate">
              <div class="categories">
              
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
              
              <div class="block-21 mb-4 d-flex">
                <div class="text"> 
                </div>
              </div>
              <div class="block-21 mb-4 d-flex">

                <div class="text">
                  <div class="meta">
                  </div>
                </div>
              </div>
            </div>

            <div class="sidebar-box ftco-animate">

              <div class="tagcloud">
              </div>
            </div>

            <div class="sidebar-box ftco-animate">
            </div>
          </div>

        </div>
      </div>
    </section> <!-- .section -->

   
    <footer class="ftco-footer ftco-bg-dark ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | 프로젝트 작성자 <i class="icon-heart" aria-hidden="true"></i> <a href="https://colorlib.com" target="_blank">주명훈</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>
    
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="../../resources/js/jquery.min.js"></script>
  <script src="../../resources/js/jquery-migrate-3.0.1.min.js"></script>
  <script src="../../resources/js/popper.min.js"></script>
  <script src="../../resources/js/bootstrap.min.js"></script>
  <script src="../../resources/js/jquery.easing.1.3.js"></script>
  <script src="../../resources/js/jquery.waypoints.min.js"></script>
  <script src="../../resources/js/jquery.stellar.min.js"></script>
  <script src="../../resources/js/owl.carousel.min.js"></script>
  <script src="../../resources/js/jquery.magnific-popup.min.js"></script>
  <script src="../../resources/js/aos.js"></script>
  <script src="../../resources/js/jquery.animateNumber.min.js"></script>
  <script src="../../resources/js/bootstrap-datepicker.js"></script>
  <script src="../../resources/js/jquery.timepicker.min.js"></script>
  <script src="../../resources/js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="../../resources/js/google-map.js"></script>
  <script src="../../resources/js/main.js"></script>
    
  </body>
</html>