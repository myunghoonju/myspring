$(function(){
	$("#changePw").validate({
		
		rules:{			
			password_now:{
				required:true,
				validPwd : true
			},
			password_after:{
				required:true,
				validPwd : true				
			},
			retype_password_after:{
				required:true,
				validPwd:true
			}
		},
		messages:{
			password_now:{
				required: "필수 입력 요소입니다."
			},
			password_after:{
				required:"필수 입력 요소입니다."			
			},
			retype_password_after:{
				required:"필수 입력 요소입니다."						
			},
			
		},

		errorPlacement:function(error,element){
			$(element).closest("form").find("small[id='"+element.attr("id")+"']").append(error);
		}
	});
});
$.validator.addMethod("validPwd", function(value) {
	var regPwd=/^(?=^[A-Z])(?=.*[@$!%*#?&])[a-zA-Z\d}{@$!'%*#?&]{6,128}$/; 
	return regPwd.test(value);
}, '비밀번호는 문자,숫자,특수문자를 사용하여 6~128자까지 가능합니다.(첫글자는 대문자)');