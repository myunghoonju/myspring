var replyService = (function(){
	function add(param, callback){
		console.log("add를 실행합니다." + param);
		$.ajax({
			url : '/reply/insert',
			dataType : 'json',
			contentType : 'application/json; charset=utf-8',
			type :'post',
			data : JSON.stringify(param),
			success : function(result){ // <-여기
				if(callback){
					callback(result); //function이 있으면 실행: success
					window.location.href = "/board/list";
				}
			}
		})
	}
	function update(param,callback){
		console.log("edit를 실행합니다.");
		var bno = param.bno;
		$.ajax({
			url:'/reply/' + bno,
			contentType:"application/json;charset=UTF-8",
			type:'patch',
			data:JSON.stringify(param),
			success:function(result){
				if(callback){
					callback(result);
				}
			}
			
		})
	}
	function remove(param,callback){
		console.log("remove를 시작합니다.")
		var bno = param.bno;
		$.ajax({
			url:'/reply/'+ bno,
			contentType:"application/json;charset=UTF-8",
			type:'delete',
			data:JSON.stringify(param),
			success:function(result){
				if(callback)
					callback(result);
				
				}
		})
	}
	
	
	return {
		add : add, //replyService 객체에 add함수 추가
		update : update,
		remove : remove
	};
})(); //익명함수();