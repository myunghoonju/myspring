/**
 * joinForm.jsp 유효성 검증하기
 */
$(function(){
	$("#joinForm").validate({
		//규칙지정
		rules:{			
			uid:{
				required:true,
				validId : true
			},
			password:{
				required:true,
				validPwd : true				
			},
			name:{
				required:true,
			},
			email:{
				required:true,
				ddIemail:true //사내메일만 허용
			},
			position:{
				required:true,
				ddIposition:true // 실장,팀장,사원
			},
			dept:{
				required:true,
				ddIdept:true //피플팀, 경영지원, 슬롯R&D
			}
			
		},
		//규칙에 위배되는 경우 보여줄 메세지 작성
		messages:{
			uid:{
				required: "필수 입력 요소입니다."
			},
			password:{
				required:"필수 입력 요소입니다."			
			},
			email:{
				required:"필수 입력 요소입니다."						
			},
			name:{
				required:"필수 입력 요소입니다."		
			},
			position:{
				required:"필수 입력 요소입니다."				
			},
			dept:{
				required:"필수 입력 요소입니다."				
			},
			
		},
		//에러 보여주는 위치 지정
		errorPlacement:function(error,element){
			$(element).closest("form").find("small[id='"+element.attr("id")+"']").append(error);
		}
	});
});
$.validator.addMethod("validId", function(value) {
	var regId=/(?=.*^[A-Za-z])(?=.*\d)[A-Za-z\d]{4,12}/;
	return regId.test(value);
}, '아이디는 문자,숫자를 사용하여 5~12자리까지 사용가능합니다.');
$.validator.addMethod("ddIemail", function(value,element) { //this.optional()은 위에 required와 대조되나 뒤에 조건 만족을 위해 필요함
	return this.optional(element) || /^.+@kr.doubledown.com$/.test(value) || /^.+@doubledown.com$/.test(value);
}, '사내메일만 가능합니다.');
$.validator.addMethod("ddIposition", function(value,element) { //this.optional()은 위에 required와 대조되나 뒤에 조건 만족을 위해 필요함
	return this.optional(element) || /^실장$/.test(value) || /^팀장$/.test(value)|| /^사원$/.test(value);
}, '존재하지 않는 직급입니다.');
$.validator.addMethod("ddIdept", function(value,element) { //this.optional()은 위에 required와 대조되나 뒤에 조건 만족을 위해 필요함
	return this.optional(element) || /^피플팀$/.test(value) || /^경영지원$/.test(value)|| /^슬롯R&D$/.test(value);
}, '존재하지 않는 부서입니다.');
$.validator.addMethod("validPwd", function(value) {
	var regPwd=/^(?=^[A-Z])(?=.*[@$!%*#?&])[a-zA-Z\d}{@$!'%*#?&]{6,128}$/; 
	return regPwd.test(value);
}, '비밀번호는 문자,숫자,특수문자를 사용하여 6~128자까지 가능합니다.(첫글자는 대문자)');