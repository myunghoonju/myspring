package com.ddc.myAuth.mybatis;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.ddc.myAuth.model.AuthVO;
import com.ddc.myAuth.model.UserInfoVO;
import com.ddc.myUser.model.FileVO;

@Repository
public class JoinDAO {
	
	@Resource(name = "sqlSessionCommon2")
	private SqlSession sqlSession;
	
	private static final String NAMESPACE = "com.ddc.myAuth.mybatis.JoinMapper."; 
	
	public int insertEmp(UserInfoVO vo) {
		return sqlSession.insert(NAMESPACE + "insertEmp", vo);
	}
	public AuthVO checkId(String uid) {
		return sqlSession.selectOne(NAMESPACE + "checkId", uid);
	}
	public List<FileVO> filelist(){
		return sqlSession.selectList(NAMESPACE + "filelist");
	}

}
