package com.ddc.myAuth.mybatis;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.ddc.myAuth.model.AuthVO;
import com.ddc.myAuth.model.ChangeInfoVO;
import com.ddc.myAuth.model.LoginVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class LoginDAO {
	
	@Resource(name = "sqlSessionCommon2")
	private SqlSession sqlSession;
	
	private static final String NAMESPACE = "com.ddc.myAuth.mybatis.LoginMapper.";
	
	public AuthVO selectByLoginInfo(LoginVO vo) {
		return  sqlSession.selectOne(NAMESPACE + "selectByLoginInfo", vo);
		
	}
	public int changePw(ChangeInfoVO vo) { //boolean으로 수정
		return sqlSession.update(NAMESPACE + "changePw", vo);
	}
	public int deletemember(AuthVO info) {
		return sqlSession.delete(NAMESPACE + "deletemember", info);
	}
	
}
