package com.ddc.myAuth;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.ddc.myAuth.model.AuthVO;
import com.ddc.myAuth.model.ChangeInfoVO;
import com.ddc.myAuth.model.LoginVO;
import com.ddc.myAuth.mybatis.LoginDAO;

@Service
public class LoginBO {

	@Inject
	LoginDAO mapper;
	
	public AuthVO selectByLoginInfo(LoginVO vo) {
		return mapper.selectByLoginInfo(vo);
	}
	public int changePw(ChangeInfoVO vo) {
		return mapper.changePw(vo);
	}
	public boolean deletemember(AuthVO info) {
		return mapper.deletemember(info) == 1 ? true:false;
	}
}
