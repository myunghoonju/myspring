package com.ddc.myAuth.model;

import java.util.Date;

import lombok.Data;

@Data
public class UserInfoVO {
	private String uid;
	private String password;
	private String name;
	private String email;
	private String position;
	private String dept;
	//db입력할때 넣어주고 화면에서 입력하거나 하는 부분은 없어서 지움
	//private Date register_date; 
	//private Date update_date;	
	
}
