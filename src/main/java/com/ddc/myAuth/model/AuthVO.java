package com.ddc.myAuth.model;

import lombok.Data;

@Data
public class AuthVO {
	private String uid;
	private String name;
	private String password;
}
