package com.ddc.myAuth.model;

import lombok.Data;

@Data
public class LoginVO {
	private String uid;
	private String password_now; //나중에 비밀번호 바꿀때 비교할 값(AuthVO에 password가 저장된 값 입니다.)
}
