package com.ddc.myAuth.model;

import javax.inject.Inject;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.Data;

@Data
public class ChangeInfoVO {
	
	@Inject
	BCryptPasswordEncoder pw_encoder;
	
	private String uid;
	private String password_now; //바꾸기 전 비밀번호
	private String password_after;//바꾸는 비밀번호
	private String retype_password_after;//새 비밀번호 확인(오타 등...)

	public boolean isSamePw() {
		System.out.println(password_after +" "+ retype_password_after);
		return password_after.equals(retype_password_after);
	}

}