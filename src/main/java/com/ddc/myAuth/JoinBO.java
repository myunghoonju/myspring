package com.ddc.myAuth;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.ddc.myAuth.model.AuthVO;
import com.ddc.myAuth.model.UserInfoVO;
import com.ddc.myAuth.mybatis.JoinDAO;
import com.ddc.myUser.model.FileVO;

@Service
public class JoinBO {
	
	@Inject
	JoinDAO mapper;
	
	
	public int insertEmp(UserInfoVO vo) {
		
		return mapper.insertEmp(vo);
	}
	
	public AuthVO checkId(String uid) {
		return mapper.checkId(uid);
	}
	
	public List<FileVO> filelist(){
		System.out.println("파일목록을 선택합니다.");
		return mapper.filelist();
	}

}
