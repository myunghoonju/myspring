package com.ddc.myAuth;

import java.lang.invoke.VolatileCallSite;
import java.util.List;

import javax.inject.Inject;
import javax.security.sasl.AuthorizeCallback;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ddc.myAuth.model.AuthVO;
import com.ddc.myAuth.model.ChangeInfoVO;
import com.ddc.myAuth.model.LoginVO;
import com.ddc.myAuth.model.UserInfoVO;
import com.ddc.myUser.model.FileVO;

import lombok.extern.slf4j.Slf4j;

@SessionAttributes("info")
@Slf4j
@RequestMapping("/auth/*")
@Controller
public class LoginController {

	
	@Inject
	LoginBO loginbo;
	@Inject
	JoinBO joinbo;
	@Inject
	BCryptPasswordEncoder pw_encoder;
	
	//@RequestMapping(value = "/login", method = {RequestMethod.GET,RequestMethod.POST})
	@GetMapping("/login")
	public void loginGet() {
		log.info("login_form을 보여줍니다.");
	}
	
	@PostMapping("/login")
	public String loginPost(LoginVO vo, Model model) {
		log.info("Log-in을 시작합니다.");
		System.out.println(vo);
		
		AuthVO info = loginbo.selectByLoginInfo(vo);
		boolean match = pw_encoder.matches(vo.getPassword_now(), info.getPassword());

		if(match) {
			System.out.println(info);
			model.addAttribute("info",info);
			log.info("Login성공");
			return "redirect:/";
		}else {
			log.info("Login실패");
			return "redirect:/auth/login"; // 주소 재요청 
		}
	}
	@GetMapping("/logout")
	public String logout(SessionStatus session) {
		log.info("logOut합니다.");
		if(!session.isComplete()) {
			session.setComplete();
		}
		return "redirect:/auth/login";
	}
	
	@GetMapping("/beforeJoin")
	public void beforejoinGet() {
		log.info("보안서약을 합니다.");
	}

	@PostMapping("/join")
	public String joinCheck(String agree) {
		log.info("서약확인 합니다.");
		System.out.println(agree);
		if(agree.equals("true")) {//동의를 고르지 않으면 다시 beforeJoin
			log.info("가입을 시작합니다.");
			return "/auth/join";
		}else {
			log.info("beforeJoin으로 돌아갑니다.");
			return "redirect:/auth/beforeJoin";
		}
	}
	public boolean checkId(String uid) {
			log.info("찾는아이디:" + uid);
			
			 AuthVO result = joinbo.checkId(uid);
			System.out.println("result: " + result);
			if(result != null) {
				System.out.println("아이디가 있습니다.");
				return false;
			}else {
				System.out.println("신규아이디 입니다.");
				return true;
			}
			
	}
	@GetMapping("/signUp")
	public void signUpGet(Model model) {
		List<FileVO> filelist = joinbo.filelist();
		model.addAttribute("filelist", filelist);
	}
	@PostMapping("/signUp")
	public String signUp(UserInfoVO vo, Model model) {
		log.info("signUp에들어왔습니다.");
		log.info("넘어온 값: " + vo);
		
		boolean check = checkId(vo.getUid()); //중복아이디 확인을 위해 조회하는 메서드 구현:존재하면 false, 신규는 true
		System.out.println("check값: "  + check);

		if(check) {
			log.info("가입을 시작합니다.");
			String enpwd = pw_encoder.encode(vo.getPassword()); //encryption
			vo.setPassword(enpwd);
			int result = joinbo.insertEmp(vo);
		
			if(result > 0) {
				//가입이 완료되면 이전 소개첨부파일 목록과 닉네임 설정을 위한 id를 표시한다.
				List<FileVO> filelist = joinbo.filelist();
				model.addAttribute("filelist", filelist);
				model.addAttribute("uid", vo.getUid());
				log.info("가입성공");
				return "/auth/signUp";
			}else {
				log.info("실패");
				return "redirect:/";
			}
		}
		
		model.addAttribute("alert", check); //아이디가 있을 경우 false값을 보내줍니다.(EL태그)
		return "/auth/join";
	}
	
	@GetMapping("/changePw")
	public void changePwGet() {
		log.info("비밀번호변경 페이지로 이동합니다.");
	}
	@PostMapping("/changePw")
	public String changePwPost(@SessionAttribute("info") AuthVO info, ChangeInfoVO vo, SessionStatus session) { //비밀번호 변경이 끝나면 로그아웃
		log.info("비밀번호를 변경합니다.");
		System.out.println(info.getUid());//세션에 있는 값 가져옴
		LoginVO empInfo = new LoginVO(); //지금 입력한 비밀번호가 맞는지 확인
		empInfo.setUid(info.getUid());//조회용
		AuthVO exist  = loginbo.selectByLoginInfo(empInfo);//확인
		if(exist != null) {//테이블에 있는 사용자
			if(vo.isSamePw()) {//새로운 비밀번호와 재입력한 비밀번호 ChangeInfoVO에 비교해서 참/거짓 반환받음
				vo.setUid(info.getUid());//조회용 아이디를 함께 넣는다.(폼에서 새비밀번호만 받았음)
				vo.setPassword_after(pw_encoder.encode(vo.getPassword_after()));
				if(loginbo.changePw(vo) > 0) {
					log.info("비밀번호가 변경되었습니다.");
					if(!session.isComplete()) {
						session.setComplete();
						return "redirect:/auth/login";
					}
				}else {
					log.info("변경을 실패하였습니다.");
					return "auth/changePw";
				}
			}
		}
		
		//기존 비밀번호 입력오류 및 새로운 비밀번호 불일치
		return "redirect:/";
	}
	@GetMapping("/leave")
	public String leavePost(@SessionAttribute("info") AuthVO info,SessionStatus session) {
		log.info("회원탈퇴");
		System.out.println("넘어온 정보: "  + "pw" + info.getPassword() + "id" + info.getUid());
			info.setPassword(pw_encoder.encode(info.getPassword()));
			boolean result = loginbo.deletemember(info);
			if(result) {
				if(!session.isComplete()) {
					session.setComplete();
				}
				return "redirect:/auth/login";
			}else {
				log.info("변경을 실패하였습니다.");
				return "redirect:/auth/login";
			}
			
			
		
	}
	
}
