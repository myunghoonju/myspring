package com.ddc.common.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ddc.myAuth.model.UserInfoVO;
import com.ddc.myUser.model.BoardVO;
import com.ddc.myUser.model.FileVO;

@Component("fileconfig")
public class FileConfig {
		
	//private static final String filePath = "C:\\Users\\main\\projects\\myProject\\src\\main\\webapp\\resources\\image\\";
	
	public List<Map<String, Object>> fileInfo(FileVO vo, MultipartHttpServletRequest mtpreq,HttpServletRequest req){
		//맵은 리스트처럼 순차적으로 접근하기 어려움 while로 반복
		Iterator<String> iterator = mtpreq.getFileNames();
		
		MultipartFile mpFile = null; //upload한 파일
		String filenameOrigin = null; 
		String fileextensionOrigin = null;
		String savedfilename = null;
		
		List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
		Map<String, Object> listMap = null;
		
		String filepath = req.getSession().getServletContext().getRealPath("/");
		File file = new File(filepath); //파일 경로와 이름을 담는 객체
		if(file.exists() == false) { //파일경로존재여부 확인
			file.mkdirs(); //존재하지 않는경우 새로 생성 .mkdir()과의 차이
		}
		
		while (iterator.hasNext()) {//있으면 반환, next()는 exception발생
			mpFile = mtpreq.getFile(iterator.next());
			if(mpFile.isEmpty() == false) {
				filenameOrigin = mpFile.getOriginalFilename();
				fileextensionOrigin = filenameOrigin.substring(filenameOrigin.lastIndexOf('.'));
				UUID uuid = UUID.randomUUID(); //저장하는 파일이름 생성
				savedfilename = uuid+fileextensionOrigin;
				
				file = new File(filepath + savedfilename); //이름.확장자
				try {
					mpFile.transferTo(file); //업로드한 파일을 생성한 경로에 저장한다.
					listMap = new HashMap<String, Object>();
					listMap.put("nickname", vo.getNickname());
					listMap.put("summary", vo.getSummary());
					listMap.put("filenameOrigin", filenameOrigin);
					listMap.put("savedfilename",savedfilename);
					listMap.put("filesize",mpFile.getSize());
					list.add(listMap);
				}catch (Exception e) {
					e.printStackTrace();
					System.out.println("파일저장오류입니다.");
				}	
				
			}//if문 종료
			
		}//while문 종료
		
	return list;
		
	}//fileInfo클래스
	
}
