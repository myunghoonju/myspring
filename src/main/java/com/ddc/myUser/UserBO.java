package com.ddc.myUser;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ddc.common.utils.FileConfig;
import com.ddc.myUser.model.BoardVO;
import com.ddc.myUser.model.Criteria;
import com.ddc.myUser.model.FileVO;
import com.ddc.myUser.model.ReplyVO;
import com.ddc.myUser.model.UserVO;
import com.ddc.myUser.mybatis.UserDAO;


@Service
public class UserBO {

	@Inject
	private UserDAO mapper;
	
	public List<UserVO> selectBoard(Criteria cri){
		return mapper.selectBoard(cri);
	}
	
	public int count_list(Criteria cri) {
		return mapper.count_list(cri);
	}
	
	public boolean insertContents(BoardVO vo) {
		return mapper.insertContents(vo);
	}
	@Transactional
	public BoardVO selectDetail(int bno) {
		System.out.println("이곳은 userbo: " + bno);
		if(mapper.updateBoard_hit(bno)) {
			return mapper.selectDetail(bno);
		}else {
			System.out.println("조회수 오류");
			return null;
		}
	}
	
	public boolean updateContents(BoardVO vo){		
		return mapper.updateContents(vo);
	}
	public boolean deleteContents(int bno) {
		return mapper.deleteContents(bno);
	}
	
	public List<ReplyVO> selectReply(int bno){
		return mapper.selectReply(bno);
	}
	
	//답글관련 기능
	@Transactional
	public boolean insertParentReply(ReplyVO vo) {
		
		if(mapper.updateReplyCount(vo.getBno())){
			return mapper.insertReply(vo);	
		}else{
			System.out.println("답글수 오류");
			return false;
		}
		
	}
	public boolean insertChildReply(ReplyVO vo) {
		return mapper.insertReply(vo);
	}
	
	public int updateBnogroup(int bno) {
		return mapper.updateBnogroup(bno);
	}
	public int updateSort(ReplyVO vo) {
		return mapper.updateSort(vo);
	}
	public int checkReplyBoard(int bno) {
		return mapper.checkReplyBoard(bno);
	}
	public int checkReReplyBoard(ReplyVO vo) {
		return mapper.checkReReplyBoard(vo);
	}
	public int updateReply(ReplyVO vo) {
		return mapper.updateReply(vo);
	}
	
	@Transactional
	public int deleteReply(ReplyVO vo) {
		System.out.println("삭제카운트부터 합니다.");
		if(mapper.updateDeleteReplyCount(vo.getBno())) {
			return	mapper.deleteReply(vo);
		}else {
			System.out.println("답글수 오류");
			return 0;
		}
	}
	
	//파일첨부관련
	public boolean uploadFile(List<Map<String, Object>> list) {
		System.out.println("파일업로드 합니다.");
		Map<String, Object> lists= null;
		
		for (int fileinfo = 0; fileinfo < list.size(); fileinfo++) {
			lists  = (list.get(fileinfo));
		}
		
		
		return mapper.uploadFile(lists);
	}
	public Map<String, String>getDownloadFileNo(int fileno){
		return mapper.getDownloadFileNo(fileno);
	}
	
	
	
}
