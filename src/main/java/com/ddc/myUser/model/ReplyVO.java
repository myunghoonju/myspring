package com.ddc.myUser.model;

import java.sql.Date;

import lombok.Data;

@Data
public class ReplyVO {
	private int rno;
	private int bno; // foreign key
	private int bnogroup; // which replier
	private int sort;//group sort
	private int depth;//replier or re-replier
	private String uid;
	private String content;
	private Date register_date;
	private Date update_date;
	
	public boolean getDepthNumber() {
		return depth == 0 ? true : false; 
	}
	
}