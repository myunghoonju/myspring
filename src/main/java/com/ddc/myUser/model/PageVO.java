package com.ddc.myUser.model;

import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class PageVO {
	//페이지 표시용도
	private int total_count;
	private int start_page;
	private int end_page;
	private boolean prev;
	private boolean next;
	private Criteria cri;
	
	
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
		calcData();
	}
	public int getStart_page() {
		return start_page;
	}
	public void setStart_page(int start_page) {
		this.start_page = start_page;
	}
	public int getEnd_page() {
		return end_page;
	}
	public void setEnd_page(int end_page) {
		this.end_page = end_page;
	}
	public boolean isPrev() {
		return prev;
	}
	public void setPrev(boolean prev) {
		this.prev = prev;
	}
	public boolean isNext() {
		return next;
	}
	public void setNext(boolean next) {
		this.next = next;
	}
	public Criteria getCri() {
		return cri;
	}
	public void setCri(Criteria cri) {
		this.cri = cri;
	}
	
	private void calcData() {
		end_page = (int)(Math.ceil(cri.getPage()/(double)cri.getPerPageNum())*cri.getPerPageNum()); //double: 0방지
		start_page = (end_page - cri.getPerPageNum()) + 1; //밑에 1부터 표시
		
		int tempEndpage = (int)(Math.ceil(total_count /(double)cri.getPerPageNum()));//마지막 페이지 눌렀을때 
		if(end_page > tempEndpage) {
			end_page = tempEndpage;
		}
		
		prev = start_page == 1 ? false : true; // start_page ==1 prev = false(None exist)
		next = end_page * cri.getPerPageNum() >= total_count ? false : true; // 7*7 >= 7 false(다음없음)
		
	}
	
	public String make_query(int page) {//보고싶은 페이지 번호가 들어오면 페이지가 몇페이지씩 보여줄지 질의확인 용도
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
										.queryParam("page", page)
										.queryParam("perPageNum", cri.getPerPageNum())
										.queryParam("searchType", cri.getSearchType())
										.queryParam("words", cri.getWords())
										.build();
		System.out.println("make_query에는: " + uriComponents.toUriString());
		return uriComponents.toUriString();
		
	}
	
}
