package com.ddc.myUser.model;

import java.sql.Date;

import lombok.Data;

@Data
public class UserVO {//게시판
	private int bno;
	private int board_hit;
	private int reply_count;
	private String title;
	private String uid;  //user_info에서 불러옴
	private String dept; //user_info에서 불러옴
	private String email;//user_info에서 불러옴
	private String content;
	private Date register_date;
	private Date update_date;
	
	
	
}