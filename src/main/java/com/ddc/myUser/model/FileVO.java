package com.ddc.myUser.model;


import java.sql.Date;

import lombok.Data;

@Data
public class FileVO{
	private int fileno;
	private String nickname;
	private String summary;
	private int filesize;
	private String filenameOrigin;
	private String filenameReal;
	private Date register_date;
	private Date update_date;
}