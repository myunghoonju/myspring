package com.ddc.myUser.model;

import lombok.Data;

@Data
public class BoardVO { //상세게시판
	private int bno;
	private String title;
	private String uid;
	private String category;
	private String content;
	private String filenameOrigin; // 업로드한 파일명
}
