package com.ddc.myUser.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Criteria {
	//가져오는 용도
	private int page; //n페이지
	private int perPageNum;//x개씩
	private int row_start; //x부터
	//private int PageStart;
	private int row_end;//
	private String searchType;
	private String words;
	
	public Criteria() {
		this.page = 1; 
		this.perPageNum = 7;
	}
	
	public void setPage(int page) {
		/*첫 페이지가 0일경우 1 아니면 해당 페이지*/
		/*
		 * if(page <= 0 ) { this.page = 1; return ; }
		 */
		this.page = page;
	}
	public void setRow_start(int row_start) {
		this.row_start = row_start;
	}

	public void setRow_end(int row_end) {
		this.row_end = row_end;
	}
	
	public void setPerPageNum(int perPageNum) {
		this.perPageNum = perPageNum;//초기값
	}
	
	public int getRow_start() {
		return (this.page-1) * perPageNum ;
	}
	public int getRow_end() {
		row_end = row_start + perPageNum; //0 + 7
		return row_end;
	}
	
	public void setRowStart(int row_start) {
		this.row_start = row_start;
	}

	public int getPage() {
		return page;
	}

	public int getPerPageNum() {
		return perPageNum;
	}
	
	//	 admin function users player model 
	@Override
	public String toString() {
		return "Criteria [page= " + page + ", perPageNum=" +perPageNum + 
				",row_start" + row_start + ",row_end" + row_end + 
				"searchType= " + searchType + "words= " + words +
				"]";
	}

	public String[] getSearchTypeA() { // null이면 빈 것 아니면 값: mapper에서 이것으로 검색어를 조회한다.
		return searchType == null ? new String[] {} : searchType.split(""); 
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getWords() {
		return words;
	}

	public void setWords(String words) {
		this.words = words;
	}
}
