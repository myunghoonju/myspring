package com.ddc.myUser;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.ddc.common.utils.FileConfig;
import com.ddc.myUser.model.BoardVO;
import com.ddc.myUser.model.Criteria;
import com.ddc.myUser.model.FileVO;
import com.ddc.myUser.model.PageVO;
import com.ddc.myUser.model.ReplyVO;
import com.ddc.myUser.model.UserVO;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/board/*")
@Controller
@Slf4j
public class UserController {
	//private static final String filePath = "C:\\Users\\main\\projects\\myProject\\src\\main\\webapp\\resources\\image\\";
	@Inject
	private UserBO userbo;
	@Inject
	private FileConfig fileconfig;
	
	@GetMapping("/list")
	public void list(Model model, Criteria cri) {
		log.debug("사원게시판 페이지로 이동합니다.");
		System.out.println("cri: " + cri.toString());

	
		  List<UserVO> list = userbo.selectBoard(cri);
		  
		  model.addAttribute("list", list);
		  
		  PageVO pagevo = new PageVO();
		  pagevo.setCri(cri); //초기값 1, 7:e.g..,[이전]8,9,10,11,12,13,14[다음]
		  pagevo.setTotal_count(userbo.count_list(cri)); //반환되는 글 수가 입력 
		  System.out.println("pagevo는" + pagevo.getStart_page());
		  System.out.println("pagevo는" + pagevo.getEnd_page());
		  System.out.println("pagevo는" + pagevo.getTotal_count());
		  System.out.println("pagevo는" + pagevo.getCri().getRow_start());
		  System.out.println("pagevo는" + pagevo.getCri().getRow_end());
		  
		  System.out.println("cri: " + cri.toString());
		  
		  model.addAttribute("pagevo", pagevo);
		  
		 
	}

	
	@GetMapping(value = { "/read", "/edit" }) // read에서는 보기만 하고 edit가서 정말 수정합니다.
	public void readGet(@RequestParam int bno, Model model) {
		log.debug("상제 글 보기 번호는?: " + bno);

		BoardVO detail = userbo.selectDetail(bno);
		List<ReplyVO> replyList = userbo.selectReply(bno);
	
		model.addAttribute("detail", detail);
		model.addAttribute("replyList", replyList);
	}

	@GetMapping("/write")
	public void writeGet() {
		log.info("글 작성 폼을 보여줍니다.");
	}

	@PostMapping("/write")
	public String writePost(BoardVO vo){
		log.info("글을 작성합니다.");
		System.out.println(vo);
		
		boolean result = userbo.insertContents(vo);
		
			if (result) {
			log.info("작성성공");

			return "redirect:/board/list";
		} else {
			log.info("작성실패");
			return "/board/write";
		}
	}

	@PostMapping("/edit")
	public String editPost(BoardVO vo) {
		log.info("게시글을 수정합니다.");
		System.out.println("넘어온 VO 입니다." + vo);

		boolean result = userbo.updateContents(vo);
		if (result) {
			log.info("수정성공");
			return "redirect:/board/list";
		} else {
			log.info("수정실패");
			return "/board/read";
		}
	}
		
	@PostMapping("/delete")
	public String deletePost(BoardVO vo) {
		log.info("해당 게시글을 삭제합니다.");
		System.out.println("넘어온 VO 입니다." + vo);

		boolean result = userbo.deleteContents(vo.getBno());
		if (result) {
			log.info("삭제성공");
			return "redirect:/board/list";
		} else {
			log.info("삭제실패");
			return "redirect:/board/read";
		}

	}
	
	@PostMapping("/upload")
	public String fileupload(FileVO vo,MultipartHttpServletRequest mtpreq, HttpServletRequest req) {
		System.out.println("파일: " +vo +"" + mtpreq +"");
		List<Map<String, Object>> list = fileconfig.fileInfo(vo, mtpreq, req);
			
			boolean result = userbo.uploadFile(list);
			if (result) {
				log.info("정상입력");
				return "redirect:/";
			} else {
				log.info("입력실패");
				return "redirect:/auth/signUp";
			}	
	}
	@PostMapping("/download")
	public void filedownload(String fileno, HttpServletResponse res, HttpServletRequest req) {
		String filepath = req.getSession().getServletContext().getRealPath("/");

		//다운로드를 진행할 파일의 실제 이름과 저장할 당시의 이름을 얻는다.
		Map<String,String> targetfile =  userbo.getDownloadFileNo(Integer.parseInt(fileno));
		
		String filenameOrigin = targetfile.get("filenameOrigin");
 		String filenameReal = targetfile.get("filenameReal");
 		
 		try {
 		//실존하는 파일의 경로와 이름을 넣어준다.
 		File realFile = new File(filepath + filenameReal);
 		//실제 파일을 읽어드려 서버로 전송하기 위해 byte배열형식으로 변환한다
 		byte sendfile[] = FileUtils.readFileToByteArray(realFile);
 		
 		//A MIME attachment
 		//서버로 파일전송 타입설정 "application/octet-stream"은 binary file이다.
 		res.setContentType("application/octet-stream");
 		//응답 파일 길이 설정
 		res.setContentLength(sendfile.length);
 		//응답지 (content-disposition: the content is expected to be displayed inline)
 		res.setHeader("Content-Disposition",  "attachment; "
 				+ "fileName=\""+URLEncoder.encode(filenameOrigin,"UTF-8")+"\";");
 		//파일변환
 		res.getOutputStream().write(sendfile);
 		//전송종료
 		res.getOutputStream().flush();
 		res.getOutputStream().close();
 		}catch(IOException e) {
 			e.printStackTrace();
 			System.out.println("파일이 존재하지 않거나 잘못된 경로");
 		}

		
	
		
		
	}
	
}
