package com.ddc.myUser;




import javax.inject.Inject;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.ddc.myUser.model.ReplyVO;


import lombok.extern.slf4j.Slf4j;

@RequestMapping("/reply")
@RestController
@Slf4j
public class UserReplyController {

	@Inject
	private UserBO userbo;

	@PostMapping("/insert")
	//@ResponseBody // 자바 객체를 HTTP 응답 몸체로 전송함(http요청 매핑) @RequestBody: HTTP 요청 몸체를 자바 객체로 전달받음	
	public ResponseEntity<String> replySave(@RequestBody ReplyVO vo) {
		System.out.println("ReplyVO: " + vo);
		
		System.out.println("bnogroup[0:댓글][1:대댓글] " + vo.getBnogroup());
		//1. 답글 입력 전에 원글에 대한 답글인지 답글에 대한 답글인지 판단한다
	
		if(vo.getDepthNumber()) {
			System.out.println("원글에 대한 답글입니다.");
			//원글에 대한 답글
			//원글에 대한 답글의 경우 입력전에 bnogroup을 +1 증가시키고 입력한다 (원글은 예전글이 큰수 최신글이 작은 수)
			if(userbo.updateBnogroup(vo.getBno()) == 0){
				//답글이 없거나 오류가 있는 경우를 구분하는 매서드가 필요하다 checkReplyBoard
				if(userbo.checkReplyBoard(vo.getBno()) == 0) { // 데이터 없음
					//최초입력 시작
					boolean result = userbo.insertParentReply(vo);
					if(result) {
						return new ResponseEntity<String>("success", HttpStatus.OK);
					}else {
						System.out.println("원글에 대한 최초답글 실패");
					}
				
									}	
			}else { //여기가 데이터 있는 경우 업데이트 된 상황
				boolean result = userbo.insertParentReply(vo);
				if(result) {// 원글을 입력 0
				return new ResponseEntity<String>("success", HttpStatus.OK);
				}else{
					System.out.println("원글에 대한 답글입력 실패");
				}
				
			}
			//원글에 대한 답글처리 끝
		//답글에 대한 답글처리 시작 (원글번호: bno, 답글번호: bnogroup으로 처리)
		}else {
			System.out.println("답글에 대한 답글입니다.");
			
			//답글에 대한 답글의 경우 입력전에 sort를 증가시킨 후 입력한다(답변은 최신글이 작은수 예전글이 큰수) bnogroup에 해당하는 원글에 대한 답변으로 저장된다.
			if(userbo.updateSort(vo) == 0) {
				//답글에 대한 답글이 없거나 문제가 있는 경우를 구분하는 매서드가 필요하다 checkReReplyBoard
				if(userbo.checkReReplyBoard(vo) == 0) {//답글에 대한 답글이 없음
					boolean result = userbo.insertChildReply(vo);//답글에 대한 답글을 입력
					if(result) {
						return new ResponseEntity<String>("success", HttpStatus.OK);
					}else {
						System.out.println("답글에 대한 답글 최초입력 실패");
					}
				}
			}else{
				boolean result = userbo.insertChildReply(vo);
				if(result) {
					//답글에 대한 답글을 입력
					return new ResponseEntity<String>("success", HttpStatus.OK);
				}else {
					System.out.println("답글에 대한 답글 입력 실패");
				}

			}//답글에 대한 답글처리 끝
		}
		//모두 끝, 하나라도 실패하면 아래 실행
		return new ResponseEntity<String>("fail",HttpStatus.BAD_REQUEST);			
	}
	//@PutMapping
	@RequestMapping(value = "/{bno}", method = {RequestMethod.PATCH})
	public ResponseEntity<String> edit(@RequestBody ReplyVO vo){
			log.info("bno, bnogroup, vo: " + vo.getBno()+ "" + vo.getBnogroup()+ "" + vo.getContent());	
		return userbo.updateReply(vo) ==1 ?
				 new ResponseEntity<String>("success",HttpStatus.OK)
				:new ResponseEntity<String>("fail",HttpStatus.BAD_REQUEST);
	}
	//@RequestMapping(method = {RequestMethod.DELETE})
	@DeleteMapping(value= "/{bno}")
	public ResponseEntity<String> remove(@RequestBody ReplyVO vo){
		log.info("삭제시작합니다: bnogroup: " + vo.getBnogroup());
		return userbo.deleteReply(vo) == 1 ?
				new ResponseEntity<String>("success",HttpStatus.OK):
				new ResponseEntity<String>("fail",HttpStatus.BAD_REQUEST);
	}
	
	
}