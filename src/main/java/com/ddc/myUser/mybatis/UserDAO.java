package com.ddc.myUser.mybatis;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.ddc.myUser.model.BoardVO;
import com.ddc.myUser.model.Criteria;
import com.ddc.myUser.model.FileVO;
import com.ddc.myUser.model.ReplyVO;
import com.ddc.myUser.model.UserVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDAO {
	
	@Resource(name = "sqlSessionCommon")
	private SqlSession sqlSession;
		
	
	private static final String NAMESPACE = "com.ddc.myUser.mybatis.UserMapper.";
	
	public List<UserVO> selectBoard(Criteria cri){
		
		return sqlSession.selectList(NAMESPACE + "selectBoard", cri);
	}
	
	public int count_list(Criteria cri) {
		return sqlSession.selectOne(NAMESPACE + "count_list", cri);
	}
	
	public boolean insertContents(BoardVO  vo) {
		return sqlSession.insert(NAMESPACE + "insertContents",vo) == 1 ? true:false;
	}
	
	public BoardVO selectDetail(int bno) {
		System.out.println("이곳은 userDAO: " + bno);
		return sqlSession.selectOne(NAMESPACE + "selectDetail", bno);
	}
	public boolean updateContents(BoardVO vo) {
		return sqlSession.update(NAMESPACE + "updateContents", vo) == 1 ? true:false;
	}
	public boolean updateBoard_hit(int bno) {
		return sqlSession.update(NAMESPACE + "updateBoard_hit",bno) == 1 ? true:false;
	}
	public boolean deleteContents(int bno) {
		return sqlSession.update(NAMESPACE + "deleteContents", bno) == 1 ? true:false;
	}
	
	//답글 관련 dao
	public List<ReplyVO> selectReply(int bno){
		System.out.println("이곳은 userDAO의 selectReplyDAO: " + bno);
		return sqlSession.selectList(NAMESPACE  + "selectReply",bno);
	}
	public boolean insertReply(ReplyVO vo) {
		System.out.println(vo);
		return sqlSession.insert(NAMESPACE + "insertReply", vo) == 1 ? true:false;
	}
	public int updateBnogroup(int bno) {
		return sqlSession.update(NAMESPACE + "updateBnogroup", bno);
	}
	public int updateSort(ReplyVO vo) {
		return sqlSession.update(NAMESPACE + "updateSort",vo);
	}
	public int checkReplyBoard(int bno) {
		return sqlSession.selectOne(NAMESPACE + "checkReplyBoard", bno);
	}
	public int checkReReplyBoard(ReplyVO vo) {
		return sqlSession.selectOne(NAMESPACE + "checkReReplyBoard", vo);
	}
	public boolean updateReplyCount(int bno) {
		return sqlSession.update(NAMESPACE + "updateReplyCount", bno) == 1 ? true : false;
	}
	public boolean updateDeleteReplyCount(int bno) {
		return sqlSession.delete(NAMESPACE + "updateDeleteReplyCount", bno) == 1? true:false;
	}
	public int updateReply(ReplyVO vo) {
		return sqlSession.update(NAMESPACE + "updateReply", vo);
	}
	public int deleteReply(ReplyVO vo) {
		return sqlSession.delete(NAMESPACE + "deleteReply", vo);
	}
	/*파일첨부관련*/
	public boolean uploadFile(Map<String, Object> lists) {
		return sqlSession.insert(NAMESPACE + "uploadFile", lists) == 1? true : false;
	}
	public Map<String,String> getDownloadFileNo(int fileno){
		return sqlSession.selectOne(NAMESPACE + "getDownloadFileNo", fileno);
	}

}